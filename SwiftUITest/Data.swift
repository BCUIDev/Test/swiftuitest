//
//  Data.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/3.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct Post: Codable, Identifiable {
	var id = UUID()
	var title: String
	var body: String
	
}

class Api {
	func getPosts(completion: @escaping ([Post]) -> ()) {
		// 保证数据格式正确,不然crash
		guard let url = URL(string: "https://jsonplaceholder.typicode.com/posts") else {return}
		
		URLSession.shared.dataTask(with: url) { (data, _, _) in
			guard let data = data else {return}
			
			let posts = try! JSONDecoder().decode([Post].self, from: data)
			// Swift UI 要求数据同步
			DispatchQueue.main.async {
				completion(posts)
			}
			
		}.resume()
		
	}
}
