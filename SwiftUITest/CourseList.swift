//
//  CourseList.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/3.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct CourseList: View {
	@ObservedObject var store = CourseStore()
	@State var active = false
	@State var activeIndex = -1
	@State var activeView = CGSize.zero
	@Environment(\.horizontalSizeClass) var horizontalSizeClass
	
    var body: some View {
		ZStack {
			Color.black.opacity(Double(self.activeView.height / 500))
				.edgesIgnoringSafeArea(.all)
				
			
			ScrollView {
				Text("Courses")
					.font(.largeTitle).bold()
					.frame(maxWidth: .infinity, alignment: .leading)
					.padding(.leading, 30)
					.padding(.top, 30)
					.blur(radius: active ? 20 : 0)
				
				VStack(spacing: 30) {
					ForEach(store.courses.indices, id: \.self) { idx in
						GeometryReader { geo in
							
							BigCourseView(active: $active,
										  course: self.$store.courses[idx],
										  size: (w: screen.width - 60, h: screen.width-60),
										  index: idx,
										  activeIndex: $activeIndex,
										  activeView: $activeView
							)
							.offset(y: self.store.courses[idx].show ? -geo.frame(in: .global).minY : 0)
							.opacity(activeIndex != idx && active ? 0 : 1)
							.scaleEffect(activeIndex != idx && active ? 0.5 : 1)
							.offset(x:activeIndex != idx && active ? screen.width : 0)
						}
						.frame(width: horizontalSizeClass == .regular ? 80 : 280) //适配iPad
						.frame(height: self.store.courses[idx].show ? screen.height : 290)
						.frame(maxWidth: self.store.courses[idx].show ? .infinity : 290)
						.zIndex(self.store.courses[idx].show ? 1 : 0)
					}
				}.frame(width: screen.width)
				.animation(.spring(response: 0.5, dampingFraction: 0.6, blendDuration: 0))
			}.statusBar(hidden: active)
			.animation(.linear)
			.gesture(
				activeIndex != -1 ? //禁止外部动画
					nil :
					DragGesture().onChanged{ value in
						//					guard value.translation.height < 300 else {return}
						self.activeView = value.translation.height < 300 ? value.translation : self.activeView
					}.onEnded { value in
						if self.activeView.height > 50 {
							self.activeIndex = -1
						}
						self.activeView = .zero
					}
					
					
			)
			
		}
			
		
    }
}

func getCardWidth(bounds: GeometryProxy) -> CGFloat {
	if bounds.size.width > 712 {
		return 712
	}
	return bounds.size.width - 60
}

struct CourseList_Previews: PreviewProvider {
    static var previews: some View {
        CourseList()
    }
}

struct BigCourseView: View {
	@Binding var active: Bool
	@Binding var course: Course
	var size: (w: CGFloat, h:CGFloat) = (250, 250)
	var index: Int
	@Binding var activeIndex: Int
	@Binding var activeView: CGSize
	
	var body: some View {
		ZStack(alignment: .top) {
			VStack(alignment: .leading, spacing: 30.0) {
				Text("Take your SwiftUI app to the App Store with advanced techniques like API data, pakages and CMS.")
				Text("About this Course")
					.font(.title).bold()
				Text("Take your SwiftUI app to the App Store with advanced techniques like API data, pakages and CMS.Take your SwiftUI app to the App Store with advanced techniques like API data, pakages and CMS.Take your SwiftUI app to the App Store with advanced techniques like API data, pakages and CMS.Take your SwiftUI app to the App Store with advanced techniques like API data, pakages and CMS.Take your SwiftUI app to the App Store with advanced techniques like API data, pakages and CMS.Take your SwiftUI app to the App Store with advanced techniques like API data, pakages and CMS.")
				Text("Take your SwiftUI app to the App Store with advanced techniques like API data, pakages and CMS.")
			}.padding(30)
			.frame(maxWidth:course.show ? .infinity : size.w-30, maxHeight: course.show ? .infinity : size.h-10, alignment: .top)
			.offset(y: course.show ? 460 : 0)
			.background(Color.white)
			.clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
			.shadow(color: Color.black.opacity(0.2), radius: 20, x: 0.0, y: 20)
			
			VStack {
				HStack(alignment: .top) {
					VStack(alignment: .leading, spacing: 5) {
						Text(course.title)
							.font(.system(size: course.show ? 25 : 20))
							.fontWeight(.bold)
							.foregroundColor(.white)
							.lineLimit(2)
							.padding(.trailing, 50)
						Text(course.subTitle)
							.foregroundColor(Color.white.opacity(0.7))
					}
					Spacer()
					VStack(spacing: -30.0) {
						Image(course.icon)
							.resizable()
							.frame(width: 36, height: 36)
							.opacity(course.show ? 0 : 1)
							.clipShape(Circle())
						Image(systemName: "xmark")
							.font(.system(size: 16, weight: .medium))
							.foregroundColor(.white).frame(width: 36, height: 36)
							.background(Color.black)
							.clipShape(Circle())
							.opacity(course.show ? 1 : 0)
					}
				}
				Spacer()
				WebImage(url: URL(string: course.image)) //图像下载 1.导入包 2.使用
					.resizable()
					.renderingMode(.original)
					.aspectRatio(contentMode: .fit)
					.padding(.bottom, course.show ? 30 : 10)
				
				
			}.padding(course.show ? 30 : 20)
			.padding(.top, course.show ? 30 : 5)
			.background(course.color)
			.cornerRadius(40)
//			.shadow(color: course.showdowColor, radius: 10, x: 0.0, y: 10 )
			.frame(width: course.show ? screen.width : size.w, height:course.show ? 460 : size.h)
			.gesture(
				course.show ? //禁止外部动画
					
				DragGesture().onChanged{ value in
//					guard value.translation.height < 300 else {return}
					self.activeView = value.translation.height < 300 ? value.translation : self.activeView
				}.onEnded { value in
					if self.activeView.height > 50 {
						course.show = false
						self.active = false
						self.activeIndex = -1
					}
					self.activeView = .zero
				}
					
				: nil
			)
			.onTapGesture {
				course.show.toggle()
				self.active.toggle()
				self.activeIndex = course.show ? index : -1
				
//				if course.show {
//					CourseDetail(course: course, active: $active, activeIndex: $activeIndex)
//						.background(Color.black)
//						.animation(nil)
//				}
				
			}
			.frame(height: course.show ? .infinity : size.h-30 )
			.scaleEffect(1 - self.activeView.height / 1000)
			.rotation3DEffect(
				Angle(degrees: Double(self.activeView.height / 5)),
				axis: (x: 0.0, y: 10.0, z: 0.0))
			.hueRotation(Angle(degrees: Double(self.activeView.height)))
			.animation(.spring(response: 0.5, dampingFraction: 0.6, blendDuration: 0.0))
			
			.edgesIgnoringSafeArea(.all)
			
		}
		
	}
	
}
