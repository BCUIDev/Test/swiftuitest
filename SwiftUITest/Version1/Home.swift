//
//  Home.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/1/27.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

let statusBarHeight = UIApplication.shared.statusBarFrame.height
let screen = UIScreen.main.bounds



struct Home: View {
	@State var show = false
	@State var showProfile = false
	@State var showModel = false
	@State var showSettings = false
	@State var showContent = false
	

    var body: some View {
		
		ZStack {
			
			HomeList(showUpdate: showProfile, showContent:$showContent)
				.blur(radius: show ? 20 : 0)
				.background(
					VStack {
						LinearGradient(gradient: Gradient(colors: [Color.gray.opacity(0.2), Color.white]), startPoint: .top, endPoint: .bottom)
							.frame(height: 200)
						Spacer()
					}.background(Color.white)
				)
				.scaleEffect(showProfile ? 0.95 : 1)
				.animation(.default)
			
			ContentView()
				.frame(minWidth: 0, maxWidth: 712)
				.background(Color.white)
				.cornerRadius(30)
				.shadow(radius: 20)
				.animation(.spring())
				.offset(y: showProfile ? statusBarHeight + 40 : UIScreen.main.bounds.height)
				.padding(.top, showProfile ? 44 : 0)
			
			
			MenuButton(show: $show)
				.offset(x: -30, y: showProfile ? statusBarHeight + 40 : 80)
				.animation(.easeOut, value: true)
			

			MenuRight(show: $showProfile, showModel: $showModel)
				.offset(x: -15, y:showProfile ? statusBarHeight + 40 : 88)
				.animation(.spring(), value: true)

			// asdas
			
			MenuView(show: $show, showSettings:$showSettings)
			
			if showContent {
				Color.white
					.edgesIgnoringSafeArea(.all)
				ContentView()
				
				VStack {
					HStack {
						Spacer()
						Image(systemName: "xmark")
							.frame(width: 36, height: 36)
							.foregroundColor(Color.black)
							.background(Color.black)
							.clipShape(Circle())
					}
					Spacer()
				}.offset(x: 16, y: 16)
				.transition(.move(edge: .top))
				.animation(.spring(response: 0.6, dampingFraction: 0.8, blendDuration: 0))
				.onTapGesture {
					self.show = false
				}
			}
			
		}.background(Color.white)
		.edgesIgnoringSafeArea(.all)
		
    }
}


struct Home_Previews: PreviewProvider {
    static var previews: some View {
		Group {
			Home().previewDevice("iPhone SE")
			Home().previewDevice("iPhone XR")
			Home().previewDevice("iPad Pro (9.7-inch)")

		}
        
    }
}

struct Menu: Identifiable {
	var id = UUID()
	var title: String
	var icon: String
}

let menuData = [
	Menu(title: "My Acount", icon: "person.crop.circle"),
	Menu(title: "Settings", icon: "gear"),
	Menu(title: "Billing", icon: "creditcard"),
	Menu(title: "Team", icon: "person.and.person"),
	Menu(title: "Sign out", icon: "arrow.uturn.down")
]


struct MenuRow: View {
	var image = "creditcard"
	var text = "My Acount"
	
	
	var body: some View {
		HStack {
			Image(systemName: image)
				.imageScale(.large)
				.foregroundColor(.yellow)
				.frame(width: 32, height: 32)
			Text(text)
				.font(.headline)
			Spacer()
			
		}.padding(.leading, 20)
	}
}


struct CircleButton1: View {
	var icon = "person.crop.circle"
	
	var body: some View {
		HStack {
			Image(systemName: icon)
				.foregroundColor(.primary)
				
		}
		.frame(width: 44, height: 44)
		.background(BlurView(style:.systemThickMaterial))
		.cornerRadius(30)
		.shadow(color: .gray, radius: 10, x: 0, y: 10)
	}
}

struct MenuButton: View {
	@Binding var show: Bool
	
	var body: some View {
		return ZStack (alignment: .topLeading)  {
			Button(action: {show.toggle() }) {
				HStack {
					Spacer()
					Image(systemName: "list.dash")
						.foregroundColor(.primary)
						
				}.padding(.trailing, 20)
				.frame(width: 90, height: 60)
				.background(BlurView(style: .systemMaterial))
				.cornerRadius(30)
				.shadow(color: .gray, radius: 10, x: 0, y: 10)
			}
			Spacer()
				.padding(0)
			
		}
	}
}

struct MenuRight: View {
	@Binding var show: Bool
	@Binding var showModel: Bool
	@EnvironmentObject var user: UserStore
	
	var body: some View {
		return ZStack (alignment: .topTrailing)  {
			HStack {
				Button(action: {show.toggle() }) {
					
					CircleButton1(icon: user.isLogged ? "" : "person.crop.circle")
					
				}

			    Button(action: {showModel.toggle() }) {
					CircleButton1(icon: "bell")
						
				}.sheet(isPresented:$showModel) {
					UpdateList()
				}
			}
			Spacer()
				.padding(.trailing, 20)
		}
	}
}
