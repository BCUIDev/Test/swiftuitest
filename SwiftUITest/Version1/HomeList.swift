//
//  HomeList.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/1/28.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct HomeList: View {
	var courses = courseData
	@State var showUpdate = false
	@Binding var showContent: Bool

    var body: some View {
		ScrollView {
			HStack {
				VStack {
					HStack {
						VStack(alignment: .leading) {
							Text("Courses")
								.font(.largeTitle)
								.fontWeight(.heavy)
							Text("22 courses")
								.foregroundColor(.gray)
						}
						Spacer()
					}
					.padding(.leading, 70.0)
					.padding(.bottom, 10)
					
					
					ScrollView(.horizontal, showsIndicators: false){
						HStack(spacing: 30) {
							Button(action: {showUpdate.toggle() }) {
								ForEach(courses) { item in
									GeometryReader { geo in
										CourseView(course: item).rotation3DEffect(
											Angle(degrees: Double(geo.frame(in: .global).minX - 30) / -40),
											axis: (x: 0.0, y: 10.0, z: 0.0))
									}
									.frame(width: 246, height: 360)
									
								}
							}.sheet(isPresented: $showUpdate, content: {
								ContentView()
							})
							
						}
						.padding(30)
						
					}
					.offset(y: -20)
					.padding(.bottom, 30)
					
					CertificateRow()
						.padding(.bottom, 60)
				}
				.padding(.top, 78)
			}
		}

		
    }
}

struct HomeList_Previews: PreviewProvider {
    static var previews: some View {
		HomeList(showUpdate: false, showContent: .constant(false))
    }
}

struct CourseView: View {
	var course: Course
	var size: (w: CGFloat, h:CGFloat) = (246, 360)
	
	var body: some View {
		VStack {
			Text(course.title)
				.font(.title)
				.fontWeight(.bold)
				.foregroundColor(.white)
				.padding(20)
				.lineLimit(4)
				.padding(.trailing, 50)
			Spacer()
			WebImage(url: URL(string: course.image))
				.resizable()
				.renderingMode(.original)
				.aspectRatio(contentMode: .fit)
				.padding(.bottom, 30)
			
		}
		.background(course.color)
		.cornerRadius(40)
		.frame(width: size.w, height: size.h)
		.shadow(color: course.showdowColor, radius: 10, x: 0.0, y: 10 )
		
	}
}


struct Course : Identifiable {
	var id = UUID()
	var title: String
	var subTitle: String
	var image: String
	var icon: String
	var color: Color
	var showdowColor: Color
	var show = false
	
}

var courseData = [
	Course(title: "Prototype Designs in SwiftUI", subTitle: "18 Sections", image: "https://t7.baidu.com/it/u=2168645659,3174029352&fm=193&f=GIF", icon:"icon", color: .red, showdowColor: .blue, show: false),
	Course(title: "SwiftUI Advanced", subTitle: "20 Sections", image: "https://t7.baidu.com/it/u=2168645659,3174029352&fm=193&f=GIF", icon:"icon", color: .red, showdowColor: .blue, show: false),
	Course(title: "UI Design for Developers", subTitle: "20 Sections", image:"https://t7.baidu.com/it/u=2168645659,3174029352&fm=193&f=GIF", icon:"icon", color: .red, showdowColor: .blue, show: false),
	Course(title: "Build an app with SwiftUI", subTitle: "22 Sections", image:"https://t7.baidu.com/it/u=2168645659,3174029352&fm=193&f=GIF", icon:"icon", color: .red, showdowColor: .blue, show: false),
	Course(title: "Build an app with Java", subTitle: "22 Sections", image: "https://t7.baidu.com/it/u=2168645659,3174029352&fm=193&f=GIF", icon:"icon", color: .red, showdowColor: .blue, show: false),
	Course(title: "Build an app with Flutter", subTitle: "22 Sections", image: "https://t7.baidu.com/it/u=2168645659,3174029352&fm=193&f=GIF", icon:"icon", color: .red, showdowColor: .blue, show: false)
]

