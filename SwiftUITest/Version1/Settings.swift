//
//  Settings.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/1.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct Settings: View {
	@State var receive = false
	@State var number = 1
	@State var selection = 1
	@State var date = Date()
	@State var email = ""
	@State var submit = false
	
    var body: some View {
		NavigationView{
			Form{
				Toggle(isOn: $receive, label: {
					Text("Receive notifications")
				})
				Stepper(value: $number, in: 1...10){
					Text("\(number) notifacation\(number > 1 ? "s" : "")")
				}
				Picker(selection: $selection, label: Text("Favorite course"), content: {
					Text("SwiftUI").tag(1)
					Text("React").tag(2)
				})
				DatePicker(selection: $date){
					Text("Date")
				}
				Section(header: Text("Email")){
					TextField("Your email", text: $email)
						
				}
				Button(action: {self.submit.toggle()}){
					Text("Submit")
				}.alert(isPresented: $submit, content: {
					Alert(title: Text("Thanks"), message: Text("Email: \(email)"))
				})
				
			}.navigationBarTitle("Settings")
		}
    }
}

struct Settings_Previews: PreviewProvider {
    static var previews: some View {
        Settings()
    }
}
