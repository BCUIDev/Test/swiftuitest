//
//  HomeView.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/1/28.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct HomeView: View {
	@State var courses = courseData
	@State var showUpdate = false
	@Binding var showContent: Bool
	@State var showCourse = false

    var body: some View {
		GeometryReader { bounds in
			ScrollView {
				HStack {
					VStack {
						HStack {
							Text("Watching")
								.font(.largeTitle)
								.fontWeight(.heavy)
							Spacer()
						}
						.padding(.leading, 70)
						.padding(.bottom, 20)
						
						ScrollView(.horizontal, showsIndicators: false) {
							WatchRingsView()
								.padding(.horizontal, 30)
								.padding(.bottom, 30)
								.onTapGesture {
									self.showContent = true
								}
						}
						
						ScrollView(.horizontal, showsIndicators: false){
							HStack(spacing: 30) {
								Button(action: {showUpdate.toggle() }) {
									ForEach(courses) { item in
										GeometryReader { geo in
											CourseView(course:item, size: (250,250)).rotation3DEffect(
												Angle(degrees: Double(geo.frame(in: .global).minX - 30) / -40),
												axis: (x: 0.0, y: 10.0, z: 0.0))
										}
										.frame(width: 250, height: 250)
										
									}
								}.sheet(isPresented: $showUpdate, content: {
									ContentView()
								})
								
							}
							.padding(20)
							
						}
						.offset(y: -20)
						.padding(.bottom, 30)
						
						CourseList()
						
						Spacer()
					}
					.padding(.top, 78)
					
				}
			}
		}

		
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
		HomeView(showUpdate: false, showContent: .constant(false))
    }
}



struct WatchRingsView: View {
	var body: some View {
		HStack(spacing: 30) {
			HStack(spacing: 10) {
				RingView(color1: .purple, color2: .secondary, width: 44, height: 44, percent: 68, show: .constant(true))
				VStack(alignment: .leading) {
					Text("6 minutes left").bold().modifier(FontModifiler(style: .subheadline))
					Text("Watched 10 mins today").modifier(FontModifiler(style: .caption))
				}.modifier(FontModifiler())
				
			}.padding(8)
			.background(Color.white)
			.cornerRadius(20)
			.modifier(ShadowModifiler())
			
			HStack(spacing: 10) {
				RingView(color1: .red, color2: .purple, width: 32, height: 32, percent: 68, show: .constant(true))
				
			}.padding(8)
			.background(Color.white)
			.cornerRadius(20)
			.modifier(ShadowModifiler())
			
			HStack(spacing: 10) {
				RingView(color1: .white, color2: .yellow, width: 32, height: 32, percent: 68, show: .constant(true))
				
			}.padding(8)
			.background(Color.white)
			.cornerRadius(20)
			.modifier(ShadowModifiler())
		}
	}
}
