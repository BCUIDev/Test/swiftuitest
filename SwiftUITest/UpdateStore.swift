//
//  UpdateStore.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/1/29.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI
import Combine

class UpdateStore: ObservableObject {
	//变量变化,自动更新
	var didChange = PassthroughSubject<Void, Never>()
	
	var updates: [Update] {
		didSet {
			didChange.send()
		}
	}
	
	init(updates: [Update] = []) {
		self.updates = updates;
	}
	
}
