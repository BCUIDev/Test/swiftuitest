//
//  CertificateRow.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/1.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct CertificateRow: View {
	var certificates = certificateData
	
	
    var body: some View {
		
		
		VStack {
			HStack {
				Text("Certificate")
					.font(.system(size: 20))
					.fontWeight(.heavy)
					.padding(.leading, 30)
				Spacer()
				
			}
			
			ScrollView(.horizontal) {
				HStack (spacing: 20) {
					ForEach(certificates) { item in
						CertificateView (item: item)
					}
				}.padding(20)
				.padding(.leading, 10)
			}
		}
		
    }
}

struct CertificateRow_Previews: PreviewProvider {
    static var previews: some View {
        CertificateRow()
    }
}

struct Certificate: Identifiable {
	var id = UUID()
	var title: String
	var image: String
	var width: Int
	var height: Int
}

let certificateData = [
	Certificate(title: "UI Design", image: "titleImage", width: 230, height: 150),
	Certificate(title: "Swift UI", image: "titleImage", width: 230, height: 150),
	Certificate(title: "Sketch", image: "titleImage", width: 230, height: 150),
	Certificate(title: "Framer", image: "titleImage", width: 230, height: 150)

]

