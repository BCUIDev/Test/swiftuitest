//
//  CourseDetail.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/3.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct CourseDetail: View {
	@State var course: Course
	@Binding var active: Bool
	@Binding var activeIndex: Int
	var size: (w: CGFloat, h:CGFloat) = (screen.width, screen.width)
	
	
    var body: some View {
		ScrollView {
			VStack {
				HStack(alignment: .top) {
					VStack(alignment: .leading, spacing: 5) {
						Text(course.title)
							.font(.system(size: course.show ? 25 : 20))
							.fontWeight(.bold)
							.foregroundColor(.white)
							.lineLimit(2)
							.padding(.trailing, 50)
						Text(course.subTitle)
							.foregroundColor(Color.white.opacity(0.7))
					}
					Spacer()
					VStack(spacing: -30.0) {
						Image(systemName: "xmark")
							.font(.system(size: 16, weight: .medium))
							.foregroundColor(.white).frame(width: 36, height: 36)
							.background(Color.black)
							.clipShape(Circle())
					}.onTapGesture {
						course.show = false
						self.active = false
						self.activeIndex = -1
					}
				}
				Spacer()
				WebImage(url: URL(string: "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.chinazzb.com%2Fuploads%2Fallimg%2F20190822%2F1566422541776_0.jpg&refer=http%3A%2F%2Fwww.chinazzb.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1614994773&t=31ce642fadc87f17cbaafcc488aff74e"))
					.resizable()
					.renderingMode(.original)
					.aspectRatio(contentMode: .fit)
					.padding(.bottom, course.show ? 30 : 10)
				
				
			}.padding(course.show ? 30 : 20)
			.padding(.top, course.show ? 30 : 5)
			.background(course.color)
			.cornerRadius(40)
			//			.shadow(color: course.showdowColor, radius: 10, x: 0.0, y: 10 )
			.frame(width: course.show ? 360 : size.w, height:course.show ? 460 : size.h)
			
			ZStack(alignment: .top) {
				VStack(alignment: .leading, spacing: 30.0) {
					Text("Take your SwiftUI app to the App Store with advanced techniques like API data, pakages and CMS.")
					Text("About this Course")
						.font(.title).bold()
					Text("Take your SwiftUI app to the App Store with advanced techniques like API data, pakages and CMS.Take your SwiftUI app to the App Store with advanced techniques like API data, pakages and CMS.")
					Text("Take your SwiftUI app to the App Store with advanced techniques like API data, pakages and CMS.")
				}.padding(30)
			}
		}.edgesIgnoringSafeArea(.all)
    }
}

struct CourseDetail_Previews: PreviewProvider {
    static var previews: some View {
		let course = Course(title: "Prototype Designs in SwiftUI", subTitle: "18 Sections", image: "illustTration1", icon:"icon", color: .red, showdowColor: .blue, show: false)
		
		CourseDetail(course: course, active: .constant(true), activeIndex: .constant(-1))
    }
}
