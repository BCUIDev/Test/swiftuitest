//
//  ContentView.swift
//  SwiftUITest
//
//  Created by BlackChen on 2020/7/29.
//  Copyright © 2020 BlackChen. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var selection = 0
	@State private var show = false
	@State private var showCard = false
	@State private var viewState = CGSize.zero
	@State private var bottomState = CGSize.zero
	@State private var showFull = false
	
	var body: some View {
		ZStack{
			BlurView(style: .systemMaterial)
			
			TitleView()
				.blur(radius: show ? 20 : 0)
		
			CardView().offset(x: 0, y:show ? -400 : -40)
				.scaleEffect(0.85)
				.rotationEffect(Angle(degrees:show ? 15 : 0))
				.rotation3DEffect(
					Angle(degrees: show ? 50 : 0),
					axis: (x: 10.0, y: 10.0, z: 10.0))
				.blendMode(.hardLight)
				.animation(.easeIn(duration: 0.7))
				.offset(viewState)


			CardView().offset(x: 0, y: show ? -200 : -20)
				.scaleEffect(0.9)
				.rotationEffect(Angle(degrees:show ? 10 : 0))
				.rotation3DEffect(
					Angle(degrees: show ? 40 : 0),
					axis: (x: 10.0, y: 10.0, z: 10.0))
				.blendMode(.multiply)
				.animation(.easeIn(duration: 0.6))
				.offset(viewState)

			CertificateView()
				.scaleEffect(0.95)
				.rotationEffect(Angle(degrees:show ? 5 : 0))
				.rotation3DEffect(
					Angle(degrees: show ? 30 : 0),
					axis: (x: 10.0, y: 10.0, z: 10.0))
				.animation(.spring(response: 1, dampingFraction: 1, blendDuration: 50))
				.onTapGesture {
					self.showCard.toggle()
				}
				.offset(viewState)
				.gesture(
					DragGesture()
						.onChanged{ value in
							self.viewState = value.translation
							self.show = true
						}
						.onEnded{ value in
							self.viewState = CGSize.zero
							self.show = false
						}
				)
			Spacer()
			GeometryReader { bounds in
 				CardBottomView(show: $showCard)
					.offset(x: 0, y: showCard ? bounds.size.height/2 : bounds.size.height)
					.offset(y: bottomState.height + bounds.safeAreaInsets.top + bounds.safeAreaInsets.bottom)
					.blur(radius: show ? 20 : 0)
					.animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8))
					.gesture(
						DragGesture().onChanged{ value in
							self.bottomState = value.translation
							if self.showFull {
								self.bottomState.height += -300
							}
							if self.bottomState.height < -300 {
								self.bottomState.height = -300
							}
						}
				)
			}
//			.edgesIgnoringSafeArea(.all)
		}
			
	}
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
//        NavView()
        ContentView()
    }
}

struct CardView: View {
	var body: some View {
		VStack{
			Text("Card Back")
		}.frame(width: 340, height: 220)
		.background(Color.blue)
		.cornerRadius(10)
		.shadow(radius: 20)
	}
}

struct CertificateView: View {
	var item = Certificate(title: "UI Design", image: "Background", width: 340, height: 220)
	
	var body: some View {
		VStack{
			HStack{
				VStack(alignment: .leading){
					Text(item.title)
						.font(.title)
						.fontWeight(.bold)
						.padding(.top)
						.foregroundColor(Color("primary"))
					Text("Certificate")
						.foregroundColor(Color.green)
					
				}
				Spacer()
				Image("").resizable().frame(width: 30, height: 30)
			}
			.padding(.horizontal)
			Spacer()
			Image(item.image).resizable().frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .center)
		}
		.frame(width: CGFloat(item.width), height: CGFloat(item.height), alignment: .center)
		.background(Color.black)
		.cornerRadius(10)
		.shadow(radius: 10)
		
	}
}

struct TitleView: View {
	var body: some View {
		VStack {
			HStack {
				Text("Certificate")
					.font(.largeTitle)
					.fontWeight(.heavy)
				Spacer()
			}
			
			Image("titleImage").resizable().frame(width: 100, height: 100, alignment: .center)
			Spacer()
		}.padding()
	}
}

struct CardBottomView: View {
	@Binding var show: Bool
	
	var body: some View {
		VStack{
			Rectangle().frame(width: 60, height: 6, alignment: .center)
				.cornerRadius(/*@START_MENU_TOKEN@*/3.0/*@END_MENU_TOKEN@*/)
				.opacity(0.1)
			Text("Placeholder")
				.lineLimit(3)
			
			HStack(spacing: 20) {
				RingView(color1: .accentColor, color2: .blue, width: 88, height: 88, percent: 78, show: .constant(show))
					.animation(Animation.easeInOut.delay(0.3))
				VStack(alignment: .leading) {
					Text("Swift UI").fontWeight(.bold)
					
					Text("Swift UI kjadlahkljdhakjldhkjaakjdhsak")
						.font(.footnote)
						.foregroundColor(.gray)
						.lineSpacing(4)
					
				}.padding(20)
				.background(Color.white)
				.cornerRadius(20)
				.shadow(color: Color.black.opacity(0.2), radius: 20, x: 0.0, y: 10)
			}
			
			Spacer()
		}
		.frame(minWidth: 0, maxWidth: .infinity)
		.padding()
		.padding(.horizontal)
		.background(BlurView(style: .systemMaterial))
		.cornerRadius(30)
		.shadow(radius: 20)
		.offset(y: UIScreen.main.bounds.height-250)
	}
}
