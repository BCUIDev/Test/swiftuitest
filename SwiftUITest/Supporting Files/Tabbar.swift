//
//  Tabbar.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/1.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct Tabbar: View {
    var body: some View {
		TabView(selection: .constant(1)){
			Home().tabItem ({
				Image(systemName: "iconHome")
				Text("Home")
			}).tag(1)
			ContentView().tabItem ({
				Image(systemName: "iconHome")
				Text("Certificates")
			}).tag(2)
			
			Settings().tabItem ({
				Image(systemName: "iconHome")
				Text("Settings")
			}).tag(3)

		}.edgesIgnoringSafeArea(.all)
    }
}

struct Tabbar_Previews: PreviewProvider {
    static var previews: some View {
		Tabbar()
        Tabbar()
			.environment(\.colorScheme, .dark)
			.environment(\.sizeCategory, .extraLarge)
    }
}
