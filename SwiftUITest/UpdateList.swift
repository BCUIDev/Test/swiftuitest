//
//  UpdateList.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/1/29.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct UpdateList: View {
	@ObservedObject var store = UpdateStore(updates: updateData)
	//
	func addUpdate() {
		store.updates.append(Update(image: "titleImage", title: "New title", text: "New text", date: "11.28"))
		print("更新--\(store.updates.count)")

	}
	
	func move(from source: IndexSet, to des: Int) {
		store.updates.swapAt(source.first!, des)
	}
	
	var body: some View {
		NavigationView{
			
			VStack {
				
				List {
					ForEach(store.updates) { item in
						NavigationLink(destination: UpdateDetail(title: item.title, text: item.text, image: item.image)) {
							HStack(spacing: 12) {
								Image(item.image)
									.resizable()
									.frame(width: 80.0, height: 80.0)
									.cornerRadius(20)
								
								VStack(alignment: .leading) {
									Text(item.title)
										.font(.headline)
									Text(item.text)
										.lineLimit(2)
										.lineSpacing(4)
										.font(.subheadline)
										.frame(height: 50)
									Text(item.date)
										.font(.caption)
										.fontWeight(.bold)
										.foregroundColor(.gray)
								}
							}
						}
						.padding(.vertical, 8.0)
												
					}.onDelete{ indexSet in
						self.store.updates.remove(at: indexSet.first!)
					}.onMove(perform:move)
				}
				.navigationBarTitle("Updates")
				.navigationBarItems(
					leading: Button(action: addUpdate) {
						Image(systemName: "plus")
					},

					trailing:EditButton())
			}
		}
//		.navigationViewStyle(StackNavigationViewStyle) //设置大窗口列表默认样式
	}
}

struct UpdateList_Previews: PreviewProvider {
	static var previews: some View {
		UpdateList()
	}
}

struct Update: Identifiable {
	var id = UUID()
	var image: String
	var title: String
	var text: String
	var date: String
	
}

var updateData = [
	Update(image: "titleImage", title: "SwiftUI1", text: "asdaksjdhkjashdkljashdjkhaiusdhaisuhdakjhasdaksjdhkjashdkljashdjkhaiusdhaisuhdakjhasdaksjdhkjashdkljashdjkhaiusdhaisuhdakjhasdaksjdhkjashdkljashdjkhaiusdhaisuhdakjh", date: "11.26"),
	Update(image: "titleImage", title: "SwiftUI2", text: "asdaksjdhkjashdkljashdjkhaiusdhaisuhdakjhasdaksjdhkjashdkljashdjkhaiusdhaisuhdakjhasdaksjdhkjashdkljashdjkhaiusdhaisuhdakjhasdaksjdhkjashdkljashdjkhaiusdhaisuhdakjh", date: "11.26"),
	Update(image: "titleImage", title: "SwiftUI3", text: "asdaksjdhkjashdkljashdjkhaiusdhaisuhdakjhasdaksjdhkjashdkljashdjkhaiusdhaisuhdakjhasdaksjdhkjashdkljashdjkhaiusdhaisuhdakjhasdaksjdhkjashdkljashdjkhaiusdhaisuhdakjh", date: "11.26"),
	Update(image: "titleImage", title: "SwiftUI4", text: "asdaksjdhkjashdkljashdjkhaiusdhaisuhdakjhasdaksjdhkjashdkljashdjkhaiusdhaisuhdakjhasdaksjdhkjashdkljashdjkhaiusdhaisuhdakjhasdaksjdhkjashdkljashdjkhaiusdhaisuhdakjh", date: "11.26"),
	
]

