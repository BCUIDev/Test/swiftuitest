//
//  BCImageUtil.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/4.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct BCImageUtil: View {
    var body: some View {
		WebImage(url: URL(string: "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.chinazzb.com%2Fuploads%2Fallimg%2F20190822%2F1566422541776_0.jpg&refer=http%3A%2F%2Fwww.chinazzb.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1614994773&t=31ce642fadc87f17cbaafcc488aff74e"))
			.resizable().frame(width: 200, height: 200, alignment: .center)
    }
}

struct BCImageUtil_Previews: PreviewProvider {
    static var previews: some View {
        BCImageUtil()
    }
}
