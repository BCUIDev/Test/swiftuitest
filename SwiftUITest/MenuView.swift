//
//  MenuView.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/7.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct MenuView: View {
	@Binding var show: Bool
	@Binding var showSettings : Bool
	@EnvironmentObject var user: UserStore
	
	var menuItems = menuData
	
	var body: some View {
		HStack {
			VStack(alignment: .leading, spacing: 20) {
				ForEach(menuItems){ item in
					if item.title == "Settings"{
						Button(action: {showSettings.toggle()}) {
							MenuRow(image: item.icon, text: item.title)
						}.sheet(isPresented: $showSettings ){
							Settings()
						}
					} else if item.title == "Sign out"{
						Button(action: {user.showLogin.toggle()}) {
							MenuRow(image: item.icon, text: item.title)
						}.sheet(isPresented: $showSettings ){
							LoginView()
						}.onTapGesture {
							user.isLogged = false
							UserDefaults.standard.set(false, forKey: "isLogged")
							show = false
						}
					} else{
						MenuRow(image: item.icon, text: item.title)
					}
					
				}
				
				Spacer()
			}.padding(.top, 44)
			.padding(10)
			.frame(minWidth: 0, idealWidth: 360)
			.background(BlurView(style: .systemMaterial))
			.cornerRadius(30)
			.padding(.trailing, 60)
			.shadow(radius: 20)
			.animation(.easeIn)
			.offset(x:show ? 0 : -UIScreen.main.bounds.width)
			.onTapGesture {
				show.toggle()
			}
			.rotation3DEffect(
				Angle(degrees: show ? 0 : 90),
				axis: (x: 0, y: 10.0, z: 0))
			Spacer()
		}.padding(.top, statusBarHeight)
	}
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
		MenuView(show: .constant(false), showSettings: .constant(false))
			.environmentObject(UserStore())
    }
}
