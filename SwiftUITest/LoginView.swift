//
//  LoginView.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/4.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct LoginView: View {
	@State var email = ""
	@State var pwd = ""
	@State var isFocusd = false //键盘响应
	@State var showAlert = false //是否弹窗
	@State var alertMessage = "Somethings was wrong" //登陆错误信息
	@State var isLoading = false //登陆中
	@State var isSuccessful = false //登陆成功
	@EnvironmentObject var user: UserStore
	
	func hideKeyboard() {
		UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
	}
	
	func login() {
		//					showAlert = true
		hideKeyboard()
		isFocusd = false
		isLoading = true
		
//		Auth.auth().signIn() ... 见截图:FireBase1
		
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
			isLoading = false
			isSuccessful = true
			user.isLogged = true
			DispatchQueue.main.asyncAfter(deadline: .now() + 8) {
				user.showLogin = false
				isSuccessful = false
				UserDefaults.standard.set(true, forKey: "isLogged")
			}
		}
	}
	
    var body: some View {
		ZStack(alignment: .top) {
			Color("primary").edgesIgnoringSafeArea(.bottom)
				.clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
				.edgesIgnoringSafeArea(.bottom)
			
			CoverView()
			
			VStack {
				HStack {
					Image(systemName: "person.crop.circle.fill")
						.foregroundColor(.gray)
						
						.frame(width: 44, height: 44)
						.clipShape(RoundedRectangle(cornerRadius: 16, style: .continuous))
						.shadow(color: Color.black.opacity(0.15), radius: 5, x: 0, y: 5)
						.padding(.leading)
					
					TextField("Your Email".uppercased(), text: $email)
						.keyboardType(.emailAddress)
						.font(.subheadline)
//						.background(Color.orange)
						.padding(.leading)
						.frame(height: 44)
//						.background(Color.blue)
//						.textFieldStyle(RoundedBorderTextFieldStyle())
						.onTapGesture {
							isFocusd = true
						}
				}
				
				Divider().padding(.leading, 80)
				
				HStack {
					Image(systemName: "lock.fill")
						.foregroundColor(.gray)
						.frame(width: 44, height: 44)
						.clipShape(RoundedRectangle(cornerRadius: 16, style: .continuous))
						.shadow(color: Color.black.opacity(0.15), radius: 5, x: 0, y: 5)
						.padding(.leading)
					TextField("Your Password".uppercased(), text: $pwd)
						.keyboardType(.emailAddress)
						.font(.subheadline)
						.padding(.leading)
						.frame(height: 44)
					//						.textFieldStyle(RoundedBorderTextFieldStyle())
						.onTapGesture {
							isFocusd = true
						}
				}
			}.frame(height: 123)
			.frame(maxWidth: .infinity)
			.background(BlurView(style: .systemMaterial))
			.clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
			.shadow(color: Color.black.opacity(0.15), radius: 20, x: 0, y: 20)
			.padding(.horizontal)
			.offset(y: screen.height * 0.6)
			
			HStack {
				Text("Forgot password?")
					.font(.subheadline)
				
				Spacer()
				
				Button(action: {
					login()
				}) {
					Text("Login").foregroundColor(.black)
				}.padding(12)
				.padding(.horizontal, 30)
				.background(Color(#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)))
				.clipShape(RoundedRectangle(cornerRadius: 20, style: .continuous))
				.shadow(color: Color(#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)).opacity(0.3), radius: 20, x: 0, y: 20)
				.alert(isPresented: $showAlert, content: {
					Alert(title: Text("Error"), message: Text(alertMessage), dismissButton: .default(Text("OK")))
				})
			}.frame(maxWidth: .infinity,  maxHeight: .infinity, alignment: .bottom)
			.padding()
			
			if isLoading {
				LottieView(fileName: "girl-cycling")
				
					
			}
			if isSuccessful {
				SuccessView()
					.padding(.top, screen.height/2-180)
			}
			
		}.offset(y: isFocusd ? -300 : 0)
		.animation(isFocusd ? .easeInOut : nil)
		.edgesIgnoringSafeArea(.all)
		.onTapGesture {
			isFocusd = false
			hideKeyboard()
		}
		
		
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}

struct CoverView: View {
	@State var show = false
	@State var viewSize = CGSize.zero
	@State var draging = false
	
	var body: some View {
		VStack(alignment: .center) {
			GeometryReader { geo in
				Text("Learn design & code. From scratch")
					.font(.system(size: geo.size.width / 12, weight: .bold))
					.foregroundColor(.white)
					.multilineTextAlignment(.center)
			}.frame(maxWidth: screen.width - 60, maxHeight: 90)
			.padding(.top, 20)
			.offset(x: viewSize.width / 10, y: viewSize.height / 20)
			
			Text("80 hours of courses for SwiftUI, React and design tools.")
				.font(.subheadline)
				.frame(width: screen.width - 100)
				.multilineTextAlignment(.center)
				.offset(x: viewSize.width / 10, y: viewSize.height / 20)
			
			Spacer()
		}
		.frame(height: screen.height * 0.618)
		.frame(maxWidth: .infinity)
		.background(
			ZStack {
				Image(uiImage: #imageLiteral(resourceName: "illustTration1"))
					.offset(x: -10, y: 100)
					.rotationEffect(Angle(degrees: show ? 360 + 90 : 90))
					.blendMode(.plusDarker)
//					.animation(Animation.linear(duration: 3).repeatForever(autoreverses: false))
					.animation(nil)
					.onAppear {
						show = true
					}
				Image(uiImage: #imageLiteral(resourceName: "illustTration1"))
					.offset(x: 0, y: 100)
					.rotationEffect(Angle(degrees: show ? 360 : 0))
					.blendMode(.plusDarker)
//					.animation(Animation.linear(duration: 6).repeatForever(autoreverses: false))
					.animation(nil)
					.onAppear {
						show = true
					}
			},
			alignment: .bottom
			
		)
		.background(Color(#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)))
		.clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
		.rotation3DEffect(
			Angle(degrees: 10),
			axis: (x: viewSize.width, y: viewSize.height, z: 0))
		.scaleEffect(draging ? 0.9 : 1)
		.animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 1))
		.gesture(
			DragGesture().onChanged({ (value) in
				viewSize = value.translation
				draging = true
			}).onEnded({ (value) in
				draging = false
				viewSize = .zero
			})
		)
	}
}
