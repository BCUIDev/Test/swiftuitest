//
//  Home2.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/1/27.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct Home2: View {
	@State var show = false
	@State var showProfile = false
	@State var showModel = false
	@State var showSettings = false
	@State var showContent = false
	@EnvironmentObject var user: UserStore
	
    var body: some View {
		
		ZStack {
			  
			HomeView(showUpdate: showProfile, showContent:$showContent)
				.blur(radius: show ? 20 : 0)
				.background(
					VStack {
						LinearGradient(gradient: Gradient(colors: [Color.gray.opacity(0.2), Color.white]), startPoint: .top, endPoint: .bottom)
							.frame(height: 200)
						Spacer()
					}.background(Color.white)
				)
				.scaleEffect(showProfile ? 0.95 : 1)
				.animation(.default)
			
			ContentView()
				.frame(minWidth: 0, maxWidth: 712)
				.background(Color.white)
				.cornerRadius(30)
				.shadow(radius: 20)
				.animation(.spring())
				.offset(y: showProfile ? statusBarHeight + 40 : UIScreen.main.bounds.height)
				.padding(.top, showProfile ? 44 : 0)
			
			MenuButton(show: $show)
				.offset(x: -30, y: showProfile ? statusBarHeight + 40 : 80)
				.animation(.easeOut, value: true)
			
			MenuRight(show: $showProfile, showModel: $showModel)
				.offset(x: -15, y:showProfile ? statusBarHeight + 40 : 88)
				.animation(.spring(), value: true)

			
			MenuView(show: $show, showSettings:$showSettings)
			
			if user.showLogin {
				ZStack {
					LoginView()
					
					VStack {
						HStack {
							Spacer()
							
							Image(systemName: "xmark")
								.frame(width: 36, height: 36)
								.foregroundColor(.white)
								.background(Color.black)
								.clipShape(Circle())
						}
						Spacer()
					}.padding()
					.onTapGesture {
						user.showLogin = false
					}
				}
			}
			
			if showContent {
				BlurView(style: .systemMaterial).edgesIgnoringSafeArea(.all)
				ContentView()
				
				VStack {
					HStack {
						Spacer()
						Image(systemName: "xmark")
							.frame(width: 36, height: 36)
							.foregroundColor(Color.white)
							.background(Color.black)
							.clipShape(Circle())
					}
					Spacer()
				}.offset(x: -16, y: 16)
				.transition(.move(edge: .top))
				.animation(.spring(response: 0.6, dampingFraction: 0.8, blendDuration: 0))
				.onTapGesture {
					self.showContent = false
				}
			}
			
		}.background(Color.white)
		.edgesIgnoringSafeArea(.all)
		
    }
}


struct Home2_Previews: PreviewProvider {
    static var previews: some View {
//		Group {
			Home2()
//				.previewDevice("iPhone SE").environment(\.colorScheme, .light)
				.environmentObject(UserStore())

//		}
        
    }
}

