//
//  UserStore.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/7.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI
import Combine

class UserStore: ObservableObject {
	@Published var isLogged: Bool = UserDefaults.standard.bool(forKey:"isLogged") {
		didSet {
			UserDefaults.standard.set(self.isLogged, forKey: "isLogged")
		}
	}
	@Published var showLogin = false
	
	
}
