//
//  DataStore.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/3.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI
import Combine

class DataStore: ObservableObject {
	@Published var posts: [Post] = []
	
	init() {
		getPosts()
	}
	
    func getPosts() {
		Api().getPosts { (posts) in
			self.posts = posts
		}
    }
}

