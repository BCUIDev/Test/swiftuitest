//
//  CourseStore.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/3.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI
import Contentful
import Combine

let client = Client(spaceId: "0ge8zzmnbp2c", accessToken: "03010b0d79abcb655ca3fda453f2f493b5472e0aaa53664bc7dea5ef4fce2676")

func getArray(id: String, conpletion:@escaping([Entry]) -> ()) {
	let query = Query.where(contentTypeId: id)
	
	client.fetchArray(of: Entry.self, matching: query) { result in
		switch result {
			case .success(let array):
				DispatchQueue.main.async {
					conpletion(array.items)
				}
				
			case .failure(let err):
				print(err.localizedDescription)
		}
	}
	
}


class CourseStore: ObservableObject {
	@Published var courses: [Course] = courseData
	
	init() {
		self.courses.append(Course.init(title: "新增数据",
										subTitle: "副标题",
										image: "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.chinazzb.com%2Fuploads%2Fallimg%2F20190822%2F1566422541776_0.jpg&refer=http%3A%2F%2Fwww.chinazzb.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1614994773&t=31ce642fadc87f17cbaafcc488aff74e",
										icon:"icon",
										color: .purple,
										showdowColor: .blue,
										show: false))
		getArray(id: "course") { (items) in
			items.forEach { (item) in
//				print(item.fields["title"]!)
				self.courses.append(Course.init(title: item.fields["title"] as! String,
										   subTitle: item.fields["subtitle"] as! String,
										   image: "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.chinazzb.com%2Fuploads%2Fallimg%2F20190822%2F1566422541776_0.jpg&refer=http%3A%2F%2Fwww.chinazzb.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1614994773&t=31ce642fadc87f17cbaafcc488aff74e",
										   icon:"icon",
										   color: .purple,
										   showdowColor: .blue,
										   show: false))
			}
		}
	}
}
