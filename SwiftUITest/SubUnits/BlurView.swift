//
//  BlurView.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/1/29.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct BlurView: UIViewRepresentable {
	let style: UIBlurEffect.Style
	
	func makeUIView(context: UIViewRepresentableContext<BlurView>) -> UIView {
		let view = UIView(frame: .zero)
		view.backgroundColor = .clear
		
		let blurEffect = UIBlurEffect(style: style)
		let blurView = UIVisualEffectView(effect: blurEffect)
		
		blurView.translatesAutoresizingMaskIntoConstraints = false
		
		view.insertSubview(blurView, at: 0) //插入视图
		NSLayoutConstraint.activate([ //约束
			blurView.heightAnchor.constraint(equalTo: view.heightAnchor),
			blurView.widthAnchor.constraint(equalTo: view.widthAnchor)
		])
		
		return view
		
	}
	
	
	func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<BlurView>) {
		
	}
	
}
