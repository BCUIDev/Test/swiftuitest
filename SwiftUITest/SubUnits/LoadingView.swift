//
//  LoadingView.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/5.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct LoadingView: View {
    var body: some View {
		VStack {
			LottieView(fileName: "2021")
				.frame(width: 200, height: 200)
		}
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}
