//
//  Modifilers.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/2.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct ShadowModifiler: ViewModifier {
	func body(content: Content) -> some View {
		content
			.shadow(color: Color.black.opacity(0.2), radius: 10, x: 0, y: 12)
			.shadow(color: Color.black.opacity(0.1), radius: 1, x: 0, y: 1)
	}
}

struct FontModifiler: ViewModifier {
	var style: Font.TextStyle = .body
	
	
	func body(content: Content) -> some View {
		content
			.font(.system(style, design: .rounded))
	}
}

struct CustomFontModifiler: ViewModifier { //自定义字体 第二部分 P4
//	1. 下载字体,拉入工程
//	2. 配置plist文件
//	3. 这里写Modifiler后就可使用
	var size: CGFloat = 28
	
	func body(content: Content) -> some View {
		content.font(.custom("字体名字", size: size))
	}
}
