//
//  RingView.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/2.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct RingView: View {
	var color1 = Color.white
	var color2 = Color.blue
	var width: CGFloat = 300
	var height: CGFloat = 300
	var percent: CGFloat = 82
	
	@Binding var show: Bool
	
	
    var body: some View {
		let multiplier = width / 44
		let progress = 1 - (percent / 100)
		
		ZStack {
			Circle()
				.stroke(Color.black.opacity(0.1),
						style: StrokeStyle(lineWidth: 5 * multiplier))
				.frame(width: width, height: height)
			
			Circle()
				.trim(from: show ? progress : 1, to: 1.0)
				.stroke(LinearGradient(gradient: Gradient(colors: [color1, color2]), startPoint: .topTrailing, endPoint: .bottomLeading),
						style: StrokeStyle(lineWidth: 5 * multiplier, lineCap: .round, lineJoin: .round, miterLimit: .infinity, dash: [20, 0], dashPhase: 0))
				.rotationEffect(Angle(degrees: 90))
				.rotation3DEffect(
					Angle(degrees: 180),
					axis: (x: 1.0, y: 0.0, z: 0.0))
				.frame(width: width, height: height)
				.shadow(color: color2.opacity(0.1), radius: 3 * multiplier, x: 0.0, y: 3 * multiplier)
				
			
			Text("\(Int(percent))%")
				.font(.system(size: 14 * multiplier))
				.fontWeight(.bold)
				.onTapGesture {
					self.show.toggle()
				}
		}
    }
}

struct RingView_Previews: PreviewProvider {
    static var previews: some View {
		RingView(show: .constant(true))
    }
}
