//
//  Buttons.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/4.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

// 震动反馈
func haptic(type: UINotificationFeedbackGenerator.FeedbackType) {
	UINotificationFeedbackGenerator().notificationOccurred(type)
}

func impact(style: UIImpactFeedbackGenerator.FeedbackStyle) {
	UIImpactFeedbackGenerator(style: style).impactOccurred()
}

struct Buttons: View {
	@State var tap = false
	@GestureState var gtap = false
	@State var press = false
	
	var body: some View {
		
		VStack(spacing: 50.0) {
			RectangleButton(press: $press, tap: $tap)
				.font(.system(size: 44, weight: .light))
			
			CircleButton(press: $press, tap: $tap)

			PayButton(press: $press)

			
		}.frame(maxWidth: .infinity, maxHeight: .infinity)
		.background(Color.white)
		.edgesIgnoringSafeArea(.all)
		.animation(.spring(response: 0.5, dampingFraction: 0.5, blendDuration: 0))
		
	}
}

struct Buttons_Previews: PreviewProvider {
	static var previews: some View {
		Buttons()
	}
}

struct RectangleButton: View {
	@Binding var press: Bool
	@Binding var tap: Bool
	var body: some View {
		Text("Button")
			.font(.system(size: 20, weight: .semibold, design: .rounded))
			.frame(width: 200, height: 60)
			.clipShape(RoundedRectangle(cornerRadius: 36, style: .continuous))
			
			.background(
				ZStack {
					Color(press ? "primary" : "button")
					
					RoundedRectangle(cornerRadius: 16, style: .continuous)
						.foregroundColor( press ? Color("primary") : .white)
						.blur(radius: 4)
						.offset(x: -8, y: -8)
					RoundedRectangle(cornerRadius: 16, style: .continuous)
						.fill(
							LinearGradient(gradient: Gradient(colors: [Color("button"), Color.white]), startPoint: .topLeading, endPoint: .bottomTrailing)
						)
						.foregroundColor(.white)
						.padding(2)
						.blur(radius: 2)
					
				}).clipShape(RoundedRectangle(cornerRadius: 16, style: .continuous))
			.overlay(
				HStack {
					Image(systemName: "person.crop.circle")
						.font(.system(size: 24, weight: .light))
						.foregroundColor(Color.white.opacity(press ? 0 : 1))
						.frame(width: press ? 64 : 54, height: press ? 5 : 50)
						.background(Color.purple)
						.clipShape(RoundedRectangle(cornerRadius: 16, style: .continuous))
						.shadow(color: Color.purple.opacity(0.3), radius: 10, x: 10, y: 10)
						.offset(x: press ? 70 : -10, y: press ? 16 : 0)
					Spacer()
				}
			)
			.scaleEffect(tap ? 1.2 : 1)
			.shadow(color: press ? .white : Color("button"), radius: 20, x: 20, y: 20)
			.shadow(color: press ? Color("button") : .white, radius: 20, x: -20, y: -20)
			.gesture(
				LongPressGesture().onChanged({ value in
					self.tap = true
					
					impact(style: .heavy)
					
					DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
						self.tap = false
					}
					self.press.toggle()
				}).onEnded({ (value) in
					
					haptic(type: .success)
				})
			)
	}
}

struct PayButton: View {
	@Binding var press: Bool
	@GestureState var tap = false
	
	var body: some View {
		ZStack {
			Image(systemName: "moon")
				.font(.system(size: 44, weight: .light))
				.opacity(press ? 0 : 1)
				.scaleEffect(press ? 0 : 1)
			
			Image(systemName: "moon")
				.font(.system(size: 44, weight: .light))
				.foregroundColor(.red)
				.clipShape(Rectangle().offset(y: tap ? 0 : 50))
				.opacity(press ? 0 : 1)
				.scaleEffect(press ? 0 : 1)
			
			Image(systemName: "checkmark.circle.fill")
				.font(.system(size: 44, weight: .light))
				.foregroundColor(.purple)
				.opacity(press ? 1 : 0)
				.scaleEffect(press ? 1 : 0)
			
		}.frame(width: 120, height: 120)
		.background(
			ZStack {
				LinearGradient(gradient: Gradient(colors: [Color(press ? #colorLiteral(red: 0.9999018312, green: 1, blue: 0.9998798966, alpha: 1) : #colorLiteral(red: 0.7525264621, green: 0.8177986741, blue: 0.9233356118, alpha: 1)), Color(press ? #colorLiteral(red: 0.910055697, green: 0.9301292896, blue: 0.9853791595, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))]), startPoint: .topLeading, endPoint: .bottomTrailing)
				
				Circle()
					.stroke(Color.clear, lineWidth: 5)
					.shadow(color: Color(press ? #colorLiteral(red: 0.9999018312, green: 1, blue: 0.9998798966, alpha: 1) : #colorLiteral(red: 0.7525264621, green: 0.8177986741, blue: 0.9233356118, alpha: 1)), radius: 3, x: -5, y: -5)
				
				Circle()
					.stroke(Color.clear, lineWidth: 5)
					.shadow(color: Color(press ? #colorLiteral(red: 0.910055697, green: 0.9301292896, blue: 0.9853791595, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)), radius: 3, x: 3, y: 3)
			}
		)
		.clipShape(Circle())
		.overlay(
			Circle()
				.trim(from: tap ? 0.001 : 1, to: 1.0)
				.stroke(LinearGradient(gradient: Gradient(colors: [Color.blue, Color.purple]), startPoint: /*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/, endPoint: /*@START_MENU_TOKEN@*/.trailing/*@END_MENU_TOKEN@*/),
						style: StrokeStyle(lineWidth: 5, lineCap: .round))
				.frame(width: 88, height: 88)
				.rotationEffect(Angle(degrees: 90))
				.rotation3DEffect(
					Angle(degrees: 180),
					axis: (x: 1.0, y: 0.0, z: 0.0))
				.shadow(color: Color.purple.opacity(0.3), radius: 5, x: 3, y: 0.0)
				.animation(.easeInOut)
		)
		.shadow(color: Color(press ? #colorLiteral(red: 0.9999018312, green: 1, blue: 0.9998798966, alpha: 1) : #colorLiteral(red: 0.7525264621, green: 0.8177986741, blue: 0.9233356118, alpha: 1)), radius: 20, x: -20, y: -20)
		.shadow(color: Color(press ? #colorLiteral(red: 0.910055697, green: 0.9301292896, blue: 0.9853791595, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)), radius: 20, x: 20, y: 20)
		.scaleEffect(tap ? 1.2 : 1)
		.gesture(
			LongPressGesture().updating($tap){ currState, gestState, tans in
				gestState = currState
			}.onEnded{ (value) in
				press.toggle()
			}
		)
	}
}


struct CircleButton: View {
	@Binding var press: Bool
	@Binding var tap: Bool
	
	var body: some View {
		ZStack {
			Image(systemName: "moon")
				.font(.system(size: 44, weight: .light))
				.offset(x: press ? -90 : 0, y: press ? -90 : 0)
				.rotation3DEffect(
					Angle(degrees: press ? 30 : 0),
					axis: (x: 10.0, y: 10.0, z: 0.0)
				)
			Image(systemName: "sun.max")
				.font(.system(size: 44, weight: .light))
				.offset(x: press ? 0 : 90, y: press ? 0 : 90)
				.rotation3DEffect(
					Angle(degrees: press ? 0 : 30),
					axis: (x: 10.0, y: 10.0, z: 0.0)
				)
		}.frame(width: 100, height: 100)
		.background(
			ZStack {
				LinearGradient(gradient: Gradient(colors: [Color(press ? #colorLiteral(red: 0.9999018312, green: 1, blue: 0.9998798966, alpha: 1) : #colorLiteral(red: 0.7525264621, green: 0.8177986741, blue: 0.9233356118, alpha: 1)), Color(press ? #colorLiteral(red: 0.910055697, green: 0.9301292896, blue: 0.9853791595, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))]), startPoint: .topLeading, endPoint: .bottomTrailing)
				
				Circle()
					.stroke(Color.clear, lineWidth: 5)
					.shadow(color: Color(press ? #colorLiteral(red: 0.9999018312, green: 1, blue: 0.9998798966, alpha: 1) : #colorLiteral(red: 0.7525264621, green: 0.8177986741, blue: 0.9233356118, alpha: 1)), radius: 3, x: -5, y: -5)
				
				Circle()
					.stroke(Color.clear, lineWidth: 5)
					.shadow(color: Color(press ? #colorLiteral(red: 0.910055697, green: 0.9301292896, blue: 0.9853791595, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)), radius: 3, x: 3, y: 3)
			}
		)
		.clipShape(Circle())
		.shadow(color: Color(press ? #colorLiteral(red: 0.9999018312, green: 1, blue: 0.9998798966, alpha: 1) : #colorLiteral(red: 0.7525264621, green: 0.8177986741, blue: 0.9233356118, alpha: 1)), radius: 20, x: -20, y: -20)
		.shadow(color: Color(press ? #colorLiteral(red: 0.910055697, green: 0.9301292896, blue: 0.9853791595, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)), radius: 20, x: 20, y: 20)
		.scaleEffect(tap ? 1.2 : 1)
		.gesture(
			LongPressGesture().onChanged{ (value) in
				tap = true
				DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
					tap = false
				}
			}.onEnded{ (value) in
				press.toggle()
			}
		)
	}
}
