//
//  SuccessView.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/7.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct SuccessView: View {
	@State var show = false
	
    var body: some View {
		ZStack {
			VStack {
				Text("Loging you...")
					.font(.title).bold()
					.opacity(show ? 1 : 0)
					.animation(Animation.linear(duration: 1).delay(0.2))
				LottieView(fileName: "2021")
					.frame(width: 300, height: 300)
					.opacity(show ? 1 : 0)
					.animation(Animation.linear(duration: 1).delay(0.4)) //需要刷新预览动画,更改任意位置就行
				
			}.background(BlurView(style: .systemMaterial))
			.clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
			.shadow(color: Color.black.opacity(0.2), radius: 30, x: 0.0, y: 30)
			.scaleEffect(show ? 1 : 0.5)
			.animation(.spring(response: 0.5, dampingFraction: 0.6, blendDuration: 0))
			.onAppear{
				show = true
			}
		}
		
    }
}

struct SuccessView_Previews: PreviewProvider {
    static var previews: some View {
        SuccessView()
    }
}
