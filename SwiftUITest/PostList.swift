//
//  PostList.swift
//  SwiftUITest
//
//  Created by BlackChen on 2021/2/3.
//  Copyright © 2021 BlackChen. All rights reserved.
//

import SwiftUI

struct PostList: View {
	@ObservedObject var store = DataStore()

	@State var posts: [Post] = []
	
    var body: some View {
		List(store.posts) { post in
			VStack(alignment: .leading, spacing: 8.0) {
				Text(post.title).font(.system(.title, design: .serif)).bold()
				Text(post.body).font(.subheadline).foregroundColor(.secondary)

			}
		}
		
//		ForEach(posts) { post in
//			Text("API" + post.title)
//				.foregroundColor(.red)
//
//		}.onAppear {
//			Api().getPosts { (posts) in
//				self.posts = posts
//			}
//		}
    }
}

struct PostList_Previews: PreviewProvider {
    static var previews: some View {
        PostList()
    }
}
